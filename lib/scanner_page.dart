import 'dart:io';

import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'scanner_manager.dart';

typedef OnScanCallback = Future<void> Function(Barcode scanData);

class ScannerPage extends StatefulWidget {
  final OnScanCallback onScan;
  final bool pauseCameraOnDetection;

  ScannerPage({
    Key key,
    this.onScan,
    this.pauseCameraOnDetection = true,
  }) : super(key: key);

  @override
  _ScannerPageState createState() => _ScannerPageState();
}

class _ScannerPageState extends State<ScannerPage> {
  ScannerManager manager;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'ScannerPage');

  static bool _scanInProgress;

  bool _cameraPaused;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller?.pauseCamera();
    }
    controller?.resumeCamera();
  }

  @override
  void initState() {
    manager = ScannerManager();
    _scanInProgress = false;
    _cameraPaused = false;
    super.initState();
  }

  @override
  void dispose() {
    _scanInProgress = false;
    _cameraPaused = false;
    manager?.clearHistory();
    super.dispose();
  }

  bool get pauseCameraOnDetection => widget.pauseCameraOnDetection;

  OnScanCallback get onScan => widget.onScan;

  Barcode get recentScan => manager.recentScan;

  Future<void> resumeCameraScanner() => controller?.resumeCamera();

  Future<void> pauseCameraScanner() => controller?.pauseCamera();

  bool _isDuplicateData(Barcode scanData) {
    final bool = scanData?.code == recentScan?.code;
    return bool;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      backgroundColor: Colors.transparent,
      bottomNavigationBar: SizedBox(
        height: 60,
        child: ListTile(
          tileColor: Colors.transparent,
          title: FutureBuilder<bool>(
            future: controller?.getFlashStatus(),
            builder: (context, snapshot) {
              var status = false;
              if (snapshot.hasData) {
                status = snapshot.data;
              }
              return ToggleButton(
                onStateToggle: (value) {
                  controller?.toggleFlash();
                },
                icon: Icon(
                  Icons.flash_on_outlined,
                  color: Colors.white,
                ),
                enabledInitially: status,
                tooltipMessage: 'Flash light',
              );
            },
          ),
        ),
      ),
      body: _buildQrView(context),
    );
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;

    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: (controller) => _onQRViewCreated(context, controller),
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
    );
  }

  void _onQRViewCreated(BuildContext context, QRViewController controller) {
    setState(() {
      this.controller = controller;
      controller.scannedDataStream.listen(_onScanListener);
    });
  }

  void _onScanListener(scanData) {
    // print('_ScannerPageState: _onQRViewCreated: scanData: ${scanData.code}');
    // print(
    //     '_ScannerPageState: _onQRViewCreated: recentScan: ${recentScan?.code}');

    // [_scanInProgress] is only true if user has kept [pauseCameraOnDetection] as true
    // & a scan is already in progress
    if (_scanInProgress) return;

    _handleScanProcess(scanData).then((_) {
      if (!_isDuplicateData(scanData)) {
        manager.storeScanResult(scanData);
      }
    });
  }

  Future<void> _handleScanProcess(scanData) {
    if (pauseCameraOnDetection) {
      // allow continuous scanning even if the [onScan] callback is executing during the scan
      return pauseCameraScanner()
          .then((_) => _scanInProgress = true)
          .then((_) => _onScan(scanData))
          .whenComplete(() => resumeCameraScanner())
          .then((_) => _scanInProgress = false);
    } else {
      // allow continuous scanning even if the [onScan] callback is executing during the scan
      return _onScan(scanData);
    }
  }

  Future<void> _onScan(Barcode scanData) async {
    // TODO: perform any action or log before scan data is processed

    return onScan?.call(scanData);
  }
}

class ToggleButton extends StatefulWidget {
  final bool enabledInitially;
  final Widget icon;
  final Color color;
  final String enabledText;
  final String tooltipMessage;
  final String disabledText;
  final ValueChanged<bool> onStateToggle;

  const ToggleButton({
    Key key,
    this.enabledInitially = false,
    @required this.icon,
    this.enabledText = 'ON',
    this.disabledText = 'OFF',
    this.tooltipMessage = 'OFF',
    this.color = Colors.white,
    this.onStateToggle,
  }) : super(key: key);

  @override
  _ToggleButtonState createState() => _ToggleButtonState();
}

class _ToggleButtonState extends State<ToggleButton> {
  bool _isEnabled;

  @override
  void initState() {
    _isEnabled = widget.enabledInitially ?? false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: widget.tooltipMessage ?? 'toggle',
      child: InkWell(
        onTap: _onTap,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Stack(
            children: [
              _isEnabled ? _buildBackground() : SizedBox(height: 50, width: 50),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Center(child: widget.icon),
                  Text(
                    _isEnabled ? widget.enabledText : widget.disabledText,
                    style: TextStyle(
                      color: widget.color,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBackground() {
    return Center(
      child: Container(
        height: 50,
        width: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.blueAccent.withOpacity(0.4),
        ),
      ),
    );
  }

  void _onTap() {
    setState(() {
      _isEnabled = !_isEnabled;
    });

    widget.onStateToggle?.call(_isEnabled);
  }
}
