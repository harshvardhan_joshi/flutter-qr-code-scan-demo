import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_qr_scan_demo/scanner_page.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

void main() => runApp(
      MaterialApp(
        builder: (context, child) {
          return FlutterEasyLoading(
            child: _buildChildAfterInit(context, child),
          );
        },
        home: QRViewExample(),
        debugShowCheckedModeBanner: false,
      ),
    );

Future<void> _initialServices(BuildContext context) async {
  WidgetsFlutterBinding.ensureInitialized();
  EasyLoading.instance.userInteractions = false;
}

Widget _buildChildAfterInit(BuildContext context, Widget child) {
  _initialServices(context);
  return child;
}

Future<void> showLoading({String message = 'Please wait'}) {
  // print(': showLoading: $trimmedStackTrace');
  return EasyLoading.show(status: message).catchError((e) {});
}

Future<void> stopLoading({bool animate = true}) {
  // print(': stopLoading: $trimmedStackTrace');
  return EasyLoading.dismiss(animation: animate).catchError((e) {});
}

class QRViewExample extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _QRViewExampleState();
}

class _QRViewExampleState extends State<QRViewExample> {
  Barcode result;
  Barcode _prevResult;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    if (Platform.isAndroid) {
      controller?.pauseCamera();
    }
    controller?.resumeCamera();
    super.reassemble();
  }

  @override
  Widget build(BuildContext context) {
    return _buildCustomBody(context);
  }

  Future<void> resumeCameraScanner() => controller?.resumeCamera();

  Future<void> pauseCameraScanner() => controller?.pauseCamera();

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: (controller) => _onQRViewCreated(context, controller),
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
    );
  }

  void _onQRViewCreated(BuildContext context, QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      print('_QRViewExampleState: _onQRViewCreated: result: ${result.code}');
      print(
          '_QRViewExampleState: _onQRViewCreated: _prevResult: ${_prevResult.code}');
      if (result.code != _prevResult.code) {
        setState(() {
          _prevResult = result;
          result = scanData;
        });
        pauseCameraScanner()
            .then((value) => _showInformation(context, result))
            .whenComplete(() => resumeCameraScanner());
      }
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  Future<void> _showInformation(BuildContext context, Barcode result) async {
    if (result == null) return;
    return showLoading()
        .then((_) => Future.delayed(Duration(seconds: 5)))
        .whenComplete(() => stopLoading())
        .then((_) => showInfoDialog(context, result));

    return showInfoDialog(context, result);
  }

  Future<void> showInfoDialog(BuildContext context, Barcode result) {
    return showDialog(
      context: context,
      builder: (dialogContext) => AlertDialog(
        title: Text('Information'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              title: Text('type: '),
              trailing: Text(result?.format.toString()),
            ),
            ListTile(
              title: Text('data: '),
              trailing: Text(result?.code),
            ),
          ],
        ),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.of(dialogContext).pop();
              },
              child: Text(MaterialLocalizations.of(context).okButtonLabel)),
        ],
      ),
    );
  }

  Widget _buildCustomBody(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: Text('Open scanner'),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) {
                return ScannerPage(
                  onScan: (scanData) {
                    print(
                        '_QRViewExampleState: _buildCustomBody: scanData: ${scanData?.code}');
                    return _showInformation(context, scanData);
                  },
                );
              },
            ));
          },
        ),
      ),
    );
  }
}
