import 'package:qr_code_scanner/qr_code_scanner.dart';

class ScannerManager {
  static const MAX_HISTORY_SIZE = 10;

  List<Barcode> _scanHistory;
  Barcode _recentScan;
  int counter;

  ScannerManager() {
    counter = 0;
    _scanHistory ??= List.empty(growable: true);
  }

  void storeScanResult(Barcode scanData) {
    if (scanData == null) return;

    _recentScan = scanData;
    _addScanToHistory(scanData);
  }

  void _addScanToHistory(Barcode scanData) {
    final index = counter % MAX_HISTORY_SIZE;
    if (_scanHistory.length < MAX_HISTORY_SIZE) {
      _scanHistory.add(scanData);
    } else {
      _scanHistory[index] = scanData;
    }
    counter++;
  }

  void clearHistory() {
    _scanHistory.clear();
  }

  Barcode get recentScan => _recentScan;

  List<Barcode> get scanHistory => List<Barcode>.from(_scanHistory);
}
